node default {
  notify { "Oops Default! I'm ${facts['hostname']}": }
}
node /win-srv\d?.node.consul/ {
  include ::profile::base_windows
  include ::profile::dns::client
  include ::profile::terminalserver
}
node /win-chocolatey\d?.node.consul/ {
  include ::profile::base_windows
  include ::profile::dns::client
  include ::profile::chocolateyserver
}
node 'manager.node.consul' {
  include ::profile::base_linux
  include ::profile::dns::client
  include ::profile::consul::server
}
