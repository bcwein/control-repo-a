# Install software to be used on the windows terminal server
class profile::terminalserver {
  include chocolatey
  $sw_pkg = lookup('terminalserver::sw_pkg')

  Package { provider => chocolatey, }

  # Ensure that the powershell script is on the server  
  file { 'C:\chocolatey-removerepo.ps1':
    ensure => file,
    source => 'puppet:///modules/profile/chocolatey-removerepo.ps1',
  }

  # execute the powershell script remotely on the server
  exec { 'C:\chocolatey-removerepo.ps1':
    provider  => powershell,
    require   => File['C:\chocolatey-removerepo.ps1'],
    logoutput => true,
  }

  # Install software packages from internal repository
  package { $sw_pkg:
    ensure  => present,
    require => Exec['C:\chocolatey-removerepo.ps1'],
    source  => 'http://win-chocolatey0/chocolatey',
  }
}

######################### Alternate solution #########################
# At the same time as the chocolatey server solution was implemented
# a solution using powershell scripts was also implemented.
# The code for the alternate solution is provideæ

# Install software to be used on the windows terminal server
# class profile::terminalserver {
#   include chocolatey
#
#   Package { provider => chocolatey, }

#   # Ensure that the powershell script is on the server
#   file { 'C:\chocolatey-test.ps1':
#     ensure => file,
#     source => 'puppet:///modules/profile/Chocolatey-test.ps1',
#   }

#   # execute the powershell script remotely on the server
#   exec { 'C:\chocolatey-test':
#   provider  => powershell,
#   require   => File('C:\chocolatey-test.ps1'),
#   logoutput => true,
#   }
#########################/Alternate solution #########################
