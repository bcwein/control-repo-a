# Chocolatey server for windows server
class profile::chocolateyserver {
$server_package_url = 'https://chocolatey.org/api/v2/'
$server_install_location = 'C:\tools\chocolatey.server'
$_chocolatey_server_app_pool_name = 'chocolateyserver'
$port = '80'
$_chocolatey_server_location = 'C:\tools\chocolatey.server'

  # package install
  package {'chocolatey.server':
    ensure   => installed,
    provider => chocolatey,
    source   => $server_package_url,
  }
  # add windows features
  iis_feature { 'Web-WebServer':
    ensure                   => present,
    include_management_tools => true,
  }
  iis_feature { 'Web-Asp-Net45':
    ensure => present,
  }
  iis_feature { 'Web-AppInit':
    ensure => present,
  }
  # remove default web site
  iis_site {'Default Web Site':
    ensure          => absent,
    applicationpool => 'DefaultAppPool',
    require         => Iis_feature['Web-WebServer'],
  }

  # application in iis
  iis_application_pool { $_chocolatey_server_app_pool_name:
    ensure                    => 'present',
    state                     => 'started',
    enable32_bit_app_on_win64 => true,
    managed_runtime_version   => 'v4.0',
    start_mode                => 'AlwaysRunning',
    idle_timeout              => '00:00:00',
    restart_time_limit        => '00:00:00',
  }
  iis_site {'chocolateyserver':
    ensure          => 'started',
    physicalpath    => $_chocolatey_server_location,
    applicationpool => $_chocolatey_server_app_pool_name,
    preloadenabled  => true,
    bindings        =>  [
      {
        'bindinginformation' => '*:80:',
        'protocol'           => 'http'
      }
    ],
    require         => Package['chocolatey.server'],
  }

  # lock down web directory
  acl { $_chocolatey_server_location:
    purge                      => true,
    inherit_parent_permissions => false,
    permissions                => [
      { identity => 'Administrators', rights => ['full'] },
      { identity => 'IIS_IUSRS', rights => ['read'] },
      { identity => 'IUSR', rights => ['read'] },
      { identity => "IIS APPPOOL\\${_chocolatey_server_app_pool_name}", rights => ['read'] }
    ],
    require                    => Package['chocolatey.server'],
  }
  acl { "${_chocolatey_server_location}/App_Data":
    permissions => [
      { identity => "IIS APPPOOL\\${_chocolatey_server_app_pool_name}", rights => ['modify'] },
      { identity => 'IIS_IUSRS', rights => ['modify'] }
    ],
    require     => Package['chocolatey.server'],
  }
  # technically you may only need IIS_IUSRS but I have not tested this yet.
}
