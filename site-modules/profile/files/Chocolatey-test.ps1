﻿$name = "vlc"
$lines = $(choco info googlechrome | Select-String "Package approved as a trusted package" | Measure-Object -Line)

if ($lines.Lines -eq 1) {
    choco install $name
    
}
else {"This is either not a Trusted Package, or you need to specify the package more precisely"}